package com.api.moneymanagerapi.controller.auth;

import com.api.moneymanagerapi.controller.ApiResponse;
import com.api.moneymanagerapi.controller.auth.request.LoginRequest;
import com.api.moneymanagerapi.controller.auth.request.RegisterRequest;
import com.api.moneymanagerapi.entity.User;
import com.api.moneymanagerapi.entity.UserLoginToken;
import com.api.moneymanagerapi.helper.StringHelper;
import com.api.moneymanagerapi.repository.UserLoginTokenRepository;
import com.api.moneymanagerapi.repository.UserRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

@RestController
@Tag(name = "Auth", description = "Controller used for login, logout")
@RequestMapping("auth")

public class AuthController {

    final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    final UserLoginTokenRepository userLoginTokenRepository;
    public AuthController(UserRepository userRepository, PasswordEncoder passwordEncoder, UserLoginTokenRepository userLoginTokenRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userLoginTokenRepository = userLoginTokenRepository;
    }

    @PostMapping("login")
    public ApiResponse<UserLoginToken> login(@RequestBody @Valid LoginRequest loginRequest){
        Optional<User> user = userRepository.findByUsername(loginRequest.getUsername());
        if (user.isEmpty()){
            return ApiResponse.<UserLoginToken>builder()
                    .errors(Map.of("username", "user not found"))
                    .build();
        }
        if( passwordEncoder.matches(loginRequest.getPassword(), user.get().getPasswordHash())){
            UserLoginToken newUserLoginToken = new UserLoginToken();
            newUserLoginToken.setToken(StringHelper.randomString(32));
            newUserLoginToken.setUser(user.get());
            newUserLoginToken.setCreatedAt(Instant.now());
            newUserLoginToken.setExpiredAt(Instant.now());
            this.userLoginTokenRepository.save(newUserLoginToken);
            return ApiResponse.<UserLoginToken>builder()
                    .data(newUserLoginToken)
                    .build();
        }
        return ApiResponse.<UserLoginToken>builder()
                .errors(Map.of("password", "password failed"))
                .build();
    }

    @PostMapping("register")
    @Transactional
    public ApiResponse<UserLoginToken> register(@RequestBody @Valid RegisterRequest registerRequest){
        Optional<User> newUser = this.userRepository.findByUsername(registerRequest.getUsername());

        if (newUser.isPresent()){
            return ApiResponse.<UserLoginToken>builder()
                    .errors(Map.of("username", "Username is already taken!"))
                    .build();
        }
        User user = new User();
        user.setUsername(registerRequest.getUsername());
        user.setEmail(registerRequest.getEmail());
        user.setPhone(registerRequest.getPhone());
        user.setName(registerRequest.getName());
        user.setPasswordHash(passwordEncoder.encode(registerRequest.getPassword()));
        user.setAddress(registerRequest.getAddress());
        user.setCreatedAt(Instant.now());
        user.setUpdatedAt(Instant.now());
        this.userRepository.save(user);

        UserLoginToken newUserLoginToken = new UserLoginToken();
        newUserLoginToken.setUser(user);
        newUserLoginToken.setCreatedAt(Instant.now());
        newUserLoginToken.setExpiredAt(Instant.now());
        newUserLoginToken.setToken(StringHelper.randomString(32));

        this.userLoginTokenRepository.save(newUserLoginToken);
        return ApiResponse.<UserLoginToken>builder()
                .data(newUserLoginToken)
                .build();
    }
}