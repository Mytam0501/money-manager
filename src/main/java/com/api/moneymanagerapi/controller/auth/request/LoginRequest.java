package com.api.moneymanagerapi.controller.auth.request;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
public class LoginRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;
}
