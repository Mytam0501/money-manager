package com.api.moneymanagerapi.controller.category;


import com.api.moneymanagerapi.controller.ApiResponse;
import com.api.moneymanagerapi.entity.ExpenseCategory;
import com.api.moneymanagerapi.entity.User;
import com.api.moneymanagerapi.repository.ExpenseCategoryRepository;
import com.api.moneymanagerapi.request.ExpenseCategoryRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@RestController
@Tag(name = "Expense Category")
@RequestMapping("expense-category")
public class ExpenseCategoryController {

    final ExpenseCategoryRepository expenseCategoryRepository;

    public ExpenseCategoryController(ExpenseCategoryRepository expenseCategoryRepository) {
        this.expenseCategoryRepository = expenseCategoryRepository;
    }

    @GetMapping("")
    @PageableAsQueryParam
    public ApiResponse<Page<ExpenseCategory>> fetchAll(@Parameter(hidden = true) Pageable pageable){
        //get the currently logged-in user
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Specification<ExpenseCategory> where = Specification.<ExpenseCategory>where(
                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("user"), user)
        );

        return ApiResponse.<Page<ExpenseCategory>>builder()
                .data(this.expenseCategoryRepository.findAll(where,pageable))
                .build();
    }

    @PostMapping("")
    public ApiResponse<ExpenseCategory> createExpenseCategory(@RequestBody @Valid ExpenseCategoryRequest expenseCategoryRequest){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        ExpenseCategory expenseCategory = new ExpenseCategory();
        expenseCategory.setUser(user);
        expenseCategory.setName(expenseCategoryRequest.getName());
        expenseCategory.setImage(expenseCategoryRequest.getImage());

        this.expenseCategoryRepository.save(expenseCategory);

        return ApiResponse.<ExpenseCategory>builder()
                .data(expenseCategory)
                .build();
    }

    @PutMapping("{id}")
    public ApiResponse<ExpenseCategory> updateExpenseCategory( @PathVariable Long id,@RequestBody @Valid ExpenseCategoryRequest expenseCategoryRequest){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<ExpenseCategory> currentExpenseCategory = this.expenseCategoryRepository.findById(id);

        if(currentExpenseCategory.isEmpty()){
            return ApiResponse.<ExpenseCategory>builder()
                    .errors(Map.of("ExpenseCategory", "category not found"))
                    .build();
        }

        ExpenseCategory newExpenseCategory = currentExpenseCategory.get();
        newExpenseCategory.setUser(user);
        newExpenseCategory.setName(expenseCategoryRequest.getName());
        newExpenseCategory.setImage(expenseCategoryRequest.getImage());

        this.expenseCategoryRepository.save(newExpenseCategory);

        return ApiResponse.<ExpenseCategory>builder()
                .data(newExpenseCategory)
                .build();
    }

    @DeleteMapping("{id}")
    public ApiResponse<ExpenseCategory> deleteExpenseCategory( @PathVariable Long id){
        Optional<ExpenseCategory> currentExpenseCategory = this.expenseCategoryRepository.findById(id);
        if(currentExpenseCategory.isEmpty()){
            return ApiResponse.<ExpenseCategory>builder()
                    .errors(Map.of("ExpenseCategory", "category not found"))
                    .build();
        }
        this.expenseCategoryRepository.delete(currentExpenseCategory.get());
        return ApiResponse.<ExpenseCategory>builder()
                .data(currentExpenseCategory.get())
                .build();
    }

}