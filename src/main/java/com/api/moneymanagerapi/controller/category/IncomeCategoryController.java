package com.api.moneymanagerapi.controller.category;


import com.api.moneymanagerapi.controller.ApiResponse;
import com.api.moneymanagerapi.entity.IncomeCategory;
import com.api.moneymanagerapi.entity.User;
import com.api.moneymanagerapi.repository.IncomeCategoryRepository;
import com.api.moneymanagerapi.request.IncomeCategoryRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@RestController
@Tag(name = "Income Category")
@RequestMapping("income-category")
public class IncomeCategoryController {
    final IncomeCategoryRepository incomeCategoryRepository;

    public IncomeCategoryController(IncomeCategoryRepository incomeCategoryRepository) {
        this.incomeCategoryRepository = incomeCategoryRepository;
    }

    @GetMapping("")
    @PageableAsQueryParam
    public ApiResponse<Page<IncomeCategory>> fetchAll(@Parameter(hidden = true) Pageable pageable){
        //get the currently logged-in user
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Specification<IncomeCategory> where = Specification.<IncomeCategory>where(
                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("user"), user)
        );


        return ApiResponse.<Page<IncomeCategory>>builder()
                .data(incomeCategoryRepository.findAll(where, pageable))
                .build();
    }

    @PostMapping("")
    public ApiResponse<IncomeCategory> createIncomeCategory(@RequestBody @Valid IncomeCategoryRequest incomeCategoryRequest){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        IncomeCategory incomeCategory = new IncomeCategory();
        incomeCategory.setUser(user);
        incomeCategory.setName(incomeCategoryRequest.getName());
        incomeCategory.setImage(incomeCategoryRequest.getImage());

        this.incomeCategoryRepository.save(incomeCategory);

        return ApiResponse.<IncomeCategory>builder()
                .data(incomeCategory)
                .build();
    }

    @PutMapping("{id}")
    public ApiResponse<IncomeCategory> updateIncomeCategory( @PathVariable Long id,@RequestBody @Valid IncomeCategoryRequest incomeCategoryRequest){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<IncomeCategory> currentIncomeCategory = this.incomeCategoryRepository.findById(id);

        if(currentIncomeCategory.isEmpty()){
            return ApiResponse.<IncomeCategory>builder()
                    .errors(Map.of("IncomeCategory", "category not found"))
                    .build();
        }

        IncomeCategory newIncomeCategory = currentIncomeCategory.get();
        newIncomeCategory.setUser(user);
        newIncomeCategory.setName(incomeCategoryRequest.getName());
        newIncomeCategory.setImage(incomeCategoryRequest.getImage());

        this.incomeCategoryRepository.save(newIncomeCategory);

        return ApiResponse.<IncomeCategory>builder()
                .data(newIncomeCategory)
                .build();
    }

    @DeleteMapping("{id}")
    public ApiResponse<IncomeCategory> deleteIncomeCategory( @PathVariable Long id){
        Optional<IncomeCategory> currentIncomeCategory = this.incomeCategoryRepository.findById(id);
        if(currentIncomeCategory.isEmpty()){
            return ApiResponse.<IncomeCategory>builder()
                    .errors(Map.of("IncomeCategory", "category not found"))
                    .build();
        }
        this.incomeCategoryRepository.delete(currentIncomeCategory.get());
        return ApiResponse.<IncomeCategory>builder()
                .data(currentIncomeCategory.get())
                .build();
    }

}
