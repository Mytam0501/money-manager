package com.api.moneymanagerapi.controller.transaction;


import com.api.moneymanagerapi.controller.ApiResponse;
import com.api.moneymanagerapi.entity.ExpenseTransaction;
import com.api.moneymanagerapi.entity.User;
import com.api.moneymanagerapi.repository.ExpenseCategoryRepository;
import com.api.moneymanagerapi.repository.ExpenseTransactionRepository;
import com.api.moneymanagerapi.request.ExpenseTransactionRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

@RestController
@Tag(name = "Expense Transaction")
@RequestMapping("expense-transaction")
public class ExpenseTransactionController {
    final ExpenseTransactionRepository expenseTransactionRepository;
    final ExpenseCategoryRepository expenseCategoryRepository;
    public ExpenseTransactionController(ExpenseTransactionRepository expenseTransactionRepository, ExpenseCategoryRepository expenseCategoryRepository) {
        this.expenseTransactionRepository = expenseTransactionRepository;
        this.expenseCategoryRepository = expenseCategoryRepository;
    }

    @GetMapping("")
    @PageableAsQueryParam
    public ApiResponse<Page<ExpenseTransaction>> fetchAll(@Parameter(hidden = true) Pageable pageable){
        //get the currently logged-in user
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Specification<ExpenseTransaction> where = Specification.<ExpenseTransaction>where(
                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("user"), user)
        );

        return ApiResponse.<Page<ExpenseTransaction>>builder()
                .data(expenseTransactionRepository.findAll(where, pageable))
                .build();
    }

    @PostMapping("")
    public ApiResponse<ExpenseTransaction> createExpenseTransaction(@RequestBody @Valid ExpenseTransactionRequest expenseTransactionRequest){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

//        Specification<ExpenseTransaction> where = Specification.<ExpenseTransaction>where(
//                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("user"), user)
//        );

        ExpenseTransaction expenseTransaction = new ExpenseTransaction();

        expenseTransaction.setUser(user);
        expenseTransaction.setAmount(expenseTransactionRequest.getAmount());
        expenseTransaction.setDate(Instant.now());
        expenseTransaction.setDescription(expenseTransactionRequest.getDescription());
        expenseTransaction.setCategory(this.expenseCategoryRepository.findById(expenseTransactionRequest.getIdCategory()).get());

        this.expenseTransactionRepository.save(expenseTransaction);

        return ApiResponse.<ExpenseTransaction>builder()
                .data(expenseTransaction)
                .build();
    }

    @PutMapping("{id}")
    public ApiResponse<ExpenseTransaction> updateExpenseTransaction( @PathVariable Long id, @RequestBody @Valid ExpenseTransactionRequest expenseTransactionRequest){
        Optional<ExpenseTransaction> currentExpenseTransaction = this.expenseTransactionRepository.findById(id);
        if(currentExpenseTransaction.isEmpty()){
            return ApiResponse.<ExpenseTransaction>builder()
                    .errors(Map.of("ExpenseTransaction", "transaction not found"))
                    .build();
        }

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        ExpenseTransaction newExpenseTransaction = currentExpenseTransaction.get();

        newExpenseTransaction.setUser(user);
        newExpenseTransaction.setAmount(expenseTransactionRequest.getAmount());
        newExpenseTransaction.setDescription(expenseTransactionRequest.getDescription());
        newExpenseTransaction.setCategory(this.expenseCategoryRepository.findById(expenseTransactionRequest.getIdCategory()).get());

        this.expenseTransactionRepository.save(newExpenseTransaction);

        return ApiResponse.<ExpenseTransaction>builder()
                .data(newExpenseTransaction)
                .build();
    }

    @DeleteMapping("{id}")
    public ApiResponse<ExpenseTransaction> deleteExpenseTransaction(@PathVariable Long id){
        Optional<ExpenseTransaction> currentExpenseTransaction = this.expenseTransactionRepository.findById(id);
        if(currentExpenseTransaction.isEmpty()){
            return ApiResponse.<ExpenseTransaction>builder()
                    .errors(Map.of("ExpenseTransaction", "transaction not found"))
                    .build();
        }
        this.expenseTransactionRepository.delete(currentExpenseTransaction.get());
        return ApiResponse.<ExpenseTransaction>builder()
                .data(currentExpenseTransaction.get())
                .build();
    }
}
