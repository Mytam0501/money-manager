package com.api.moneymanagerapi.controller.transaction;


import com.api.moneymanagerapi.controller.ApiResponse;
import com.api.moneymanagerapi.entity.IncomeTransaction;
import com.api.moneymanagerapi.entity.User;
import com.api.moneymanagerapi.repository.IncomeCategoryRepository;
import com.api.moneymanagerapi.repository.IncomeTransactionRepository;
import com.api.moneymanagerapi.request.IncomeTransactionRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

@RestController
@Tag(name = "Income Transaction")
@RequestMapping("income-transaction")
public class IncomeTransactionController {
    final IncomeTransactionRepository incomeTransactionRepository;
    final IncomeCategoryRepository incomeCategoryRepository;
    public IncomeTransactionController(IncomeTransactionRepository incomeTransactionRepository, IncomeCategoryRepository incomeCategoryRepository) {
        this.incomeTransactionRepository = incomeTransactionRepository;
        this.incomeCategoryRepository = incomeCategoryRepository;
    }

    @GetMapping("")
    @PageableAsQueryParam
    public ApiResponse<Page<IncomeTransaction>> fetchAll(@Parameter(hidden = true) Pageable pageable){
        //get the currently logged-in user
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Specification<IncomeTransaction> where = Specification.<IncomeTransaction>where(
                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("user"), user)
        );

        return ApiResponse.<Page<IncomeTransaction>>builder()
                .data(incomeTransactionRepository.findAll(where, pageable))
                .build();
    }

    @PostMapping("")
    public ApiResponse<IncomeTransaction> createIncomeTransaction(@RequestBody @Valid IncomeTransactionRequest incomeTransactionRequest){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        IncomeTransaction incomeTransaction = new IncomeTransaction();

        incomeTransaction.setUser(user);
        incomeTransaction.setAmount(incomeTransactionRequest.getAmount());
        incomeTransaction.setDate(Instant.now());
        incomeTransaction.setDescription(incomeTransactionRequest.getDescription());
        incomeTransaction.setCategory(this.incomeCategoryRepository.findById(incomeTransactionRequest.getIdCategory()).get());

        this.incomeTransactionRepository.save(incomeTransaction);

        return ApiResponse.<IncomeTransaction>builder()
                .data(incomeTransaction)
                .build();
    }

    @PutMapping("{id}")
    public ApiResponse<IncomeTransaction> updateIncomeTransaction( @PathVariable Long id, @RequestBody @Valid IncomeTransactionRequest incomeTransactionRequest){
        Optional<IncomeTransaction> currentIncomeTransaction = this.incomeTransactionRepository.findById(id);
        if(currentIncomeTransaction.isEmpty()){
            return ApiResponse.<IncomeTransaction>builder()
                    .errors(Map.of("IncomeTransaction", "transaction not found"))
                    .build();
        }

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        IncomeTransaction newIncomeTransaction = currentIncomeTransaction.get();

        newIncomeTransaction.setAmount(incomeTransactionRequest.getAmount());
        newIncomeTransaction.setDescription(incomeTransactionRequest.getDescription());
        newIncomeTransaction.setCategory(this.incomeCategoryRepository.findById(incomeTransactionRequest.getIdCategory()).get());

        this.incomeTransactionRepository.save(newIncomeTransaction);

        return ApiResponse.<IncomeTransaction>builder()
                .data(newIncomeTransaction)
                .build();
    }

    @DeleteMapping("{id}")
    public ApiResponse<IncomeTransaction> deleteIncomeTransaction(@PathVariable Long id){
        Optional<IncomeTransaction> currentIncomeTransaction = this.incomeTransactionRepository.findById(id);
        if(currentIncomeTransaction.isEmpty()){
            return ApiResponse.<IncomeTransaction>builder()
                    .errors(Map.of("IncomeTransaction", "transaction not found"))
                    .build();
        }
        this.incomeTransactionRepository.delete(currentIncomeTransaction.get());
        return ApiResponse.<IncomeTransaction>builder()
                .data(currentIncomeTransaction.get())
                .build();
    }

}
