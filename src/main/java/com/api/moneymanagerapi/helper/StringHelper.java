package com.api.moneymanagerapi.helper;

import java.util.Random;

public class StringHelper {
    public static String randomString(int length){
        String t = "0123456789ABCDEFGHIKLMNOPQRSTUVXYWZJabcdefghiklmnopqrstuvxywzj";
        StringBuilder result = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < length; i++){
            int n = random.nextInt(t.length());
            result.append(t.charAt(n));
        }
        return result.toString();
    }
}

