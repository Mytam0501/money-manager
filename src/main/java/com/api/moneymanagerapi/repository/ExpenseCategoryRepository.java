package com.api.moneymanagerapi.repository;

import com.api.moneymanagerapi.entity.ExpenseCategory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ExpenseCategoryRepository extends PagingAndSortingRepository<ExpenseCategory, Long>, JpaSpecificationExecutor<ExpenseCategory> {
}