package com.api.moneymanagerapi.repository;

import com.api.moneymanagerapi.entity.ExpenseTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ExpenseTransactionRepository extends PagingAndSortingRepository<ExpenseTransaction, Long>, JpaSpecificationExecutor<ExpenseTransaction> {
}