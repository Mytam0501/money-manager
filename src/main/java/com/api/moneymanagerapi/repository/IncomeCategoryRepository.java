package com.api.moneymanagerapi.repository;

import com.api.moneymanagerapi.entity.IncomeCategory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IncomeCategoryRepository extends PagingAndSortingRepository<IncomeCategory, Long>, JpaSpecificationExecutor<IncomeCategory> {
}