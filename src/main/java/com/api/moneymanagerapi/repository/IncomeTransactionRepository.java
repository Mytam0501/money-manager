package com.api.moneymanagerapi.repository;

import com.api.moneymanagerapi.entity.IncomeTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IncomeTransactionRepository extends PagingAndSortingRepository<IncomeTransaction, Long>, JpaSpecificationExecutor<IncomeTransaction> {
}