package com.api.moneymanagerapi.repository;

import com.api.moneymanagerapi.entity.UserLoginToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserLoginTokenRepository extends JpaRepository<UserLoginToken, Long> {
    Optional<UserLoginToken> findByToken(String token);
}