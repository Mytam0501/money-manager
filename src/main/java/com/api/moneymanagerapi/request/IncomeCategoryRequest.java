package com.api.moneymanagerapi.request;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
public class IncomeCategoryRequest {
    private String image;

    @NotNull
    private String name;

}