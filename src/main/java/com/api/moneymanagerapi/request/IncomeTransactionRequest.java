package com.api.moneymanagerapi.request;


import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
@Setter
public class IncomeTransactionRequest {
    @NotNull
    private Long amount;

    private String description;

    @NotNull
    private long idCategory;
}