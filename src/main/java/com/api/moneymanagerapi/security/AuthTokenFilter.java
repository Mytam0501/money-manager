package com.api.moneymanagerapi.security;


import com.api.moneymanagerapi.entity.UserLoginToken;
import com.api.moneymanagerapi.repository.UserLoginTokenRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthTokenFilter extends OncePerRequestFilter{
    final UserLoginTokenRepository userLoginTokenRepository;

    public AuthTokenFilter(UserLoginTokenRepository userLoginTokenRepository) {
        this.userLoginTokenRepository = userLoginTokenRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String BEARER_PREFIX = "Bearer ";
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (token == null){
            filterChain.doFilter(request, response);
            return;
        }
        token = token.substring (BEARER_PREFIX.length());

        UserLoginToken userLoginToken = userLoginTokenRepository.findByToken(token).orElse(null);
        if (userLoginToken == null) {
            filterChain.doFilter(request, response);
            return;
        }
        Set<String> permissions = Set.of("USER");
        List<SimpleGrantedAuthority> authorities = permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());

        Authentication authentication = new UsernamePasswordAuthenticationToken(userLoginToken.getUser(), token, authorities);
        SecurityContextHolder.getContext().setAuthentication (authentication);

        filterChain.doFilter(request, response);

    }
}
